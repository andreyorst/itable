{:paths {:fennel ["src/?.fnl"]}
 :profiles
 {:dev
  {:deps {"https://gitlab.com/andreyorst/fennel-test"
          {:type :git :sha "9aae4dd52be1f5dd1ee97e61c639e6c5a2f9afee"}}
   :paths {:fennel ["test/?.fnl"]}}}}
